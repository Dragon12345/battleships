using UnityEngine;
using System.Collections;

public class Battleship :BoardSquare {

	protected override void Awake () {
		base.Awake();
		totalHits=4;
		images [2] = Resources.Load ("Battleship") as Texture2D;
		bonusAmmo = 0;
	}
	
	protected override void onSquareSelected() {
		Battleship[] battleships = GameObject.FindObjectsOfType<Battleship>() as Battleship[];
		foreach(Battleship battleship in battleships) {
			if(battleship.hit && battleship.Number == this.Number) {
				totalHits--;
			}
		}		
		if(totalHits==0) {
			updateEndGraphic  (this);
			updateDestroyShip  (this);
		}
	}

	protected override void updateDestroyShip(BoardSquare ship)
	{
		GameController.instance.shipsLeftCounter();
		GameController.instance.shotsLeftCounter(bonusAmmo);
	}

	protected override void updateEndGraphic (BoardSquare ship)
	{
		Battleship[] battleships = GameObject.FindObjectsOfType<Battleship> () as Battleship[];
		foreach (Battleship battleship in battleships) {
			if (battleship.Number == ship.Number) {
				battleship.GetComponent<Renderer>().material.color = new Color (1, .5f, .5f);
				battleship.GetComponent<Renderer>().material.mainTexture = images [2];	
			}
		}
	}
}
