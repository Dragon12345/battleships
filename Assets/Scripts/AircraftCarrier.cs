using UnityEngine;
using System.Collections;

public class AircraftCarrier : BoardSquare {

	protected override void Awake () {
		base.Awake();
		totalHits = 5;
		images [2] = Resources.Load ("Carrier") as Texture2D;
		bonusAmmo = 0;
	}
	
	protected override void onSquareSelected() {
		AircraftCarrier[] aircraftCarriers = GameObject.FindObjectsOfType<AircraftCarrier>() as AircraftCarrier[];
		foreach(AircraftCarrier aircraftCarrier in aircraftCarriers) {
			if(aircraftCarrier.hit && aircraftCarrier.Number == this.Number) {
				totalHits--;
			}
		}		
		if(totalHits==0) {
			updateEndGraphic  (this);
			updateDestroyShip  (this);
		}
	}

	protected override void updateDestroyShip(BoardSquare ship)
	{
		GameController.instance.shipsLeftCounter();
		GameController.instance.shotsLeftCounter(bonusAmmo);
	}

	protected override void updateEndGraphic (BoardSquare ship)
	{
		AircraftCarrier[] carriers = GameObject.FindObjectsOfType<AircraftCarrier> () as AircraftCarrier[];
		foreach (AircraftCarrier carrier in carriers) {
			if (carrier.Number == carrier.Number) {
				carrier.GetComponent<Renderer>().material.color = new Color (1, .5f, .5f);
				carrier.GetComponent<Renderer>().material.mainTexture = images [2];	
			}
		}
	}

}
