using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

	public GameObject seaPrefab;

	public Text shipsLeftTextBox;
	public Text shotsLeftTextBox;

	public int ShipsLeft {get; set;}
	public int ShotsLeft {get; set;}

	public static GameController instance;

	private void Awake()
	{
		if (instance == null) instance = this;
		else Destroy(this.gameObject);
	}


	void Start () {
		buildLevel();

		ShipsLeft = FindObjectsOfType<AircraftCarrier>().Length / 5;
		ShipsLeft += FindObjectsOfType<Battleship>().Length / 4;
		ShipsLeft += FindObjectsOfType<Cruiser>().Length / 3;
		ShipsLeft += FindObjectsOfType<Destroyer>().Length / 2;
		ShipsLeft += FindObjectsOfType<Submarine>().Length;

		shipsLeftTextBox.text = "Ships Left: " + ShipsLeft;

		ShotsLeft = 50;

		shotsLeftTextBox.text = "Shots Left: " + ShotsLeft;
	}
	
	private void buildLevel() {
		int[] currentLevel = Globals.CURRENTLEVEL;

		int rows = 0;
		int column = 0;

		for(int i = 0; i < currentLevel.Length; i++) {
			GameObject square = Instantiate (seaPrefab) as GameObject;

			switch (currentLevel[i]){
			case 0:
				square.AddComponent<Sea>();
				break;
			case 1:
				square.AddComponent<Submarine>();
				break;
			case 2:
				square.AddComponent<Submarine>();
				break;
			case 3:
				square.AddComponent<Submarine>();
				break;
			case 4:
				square.AddComponent<Submarine>();
				break;
			case 5:
				square.AddComponent<Destroyer>();
				break;
			case 6:
				square.AddComponent<Destroyer>();
				break;
			case 7:
				square.AddComponent<Cruiser>();
				break;
			case 8:
				square.AddComponent<Cruiser>();
				break;
			case 9:
				square.AddComponent<Battleship>();
				break;
			case 10:
				square.AddComponent<AircraftCarrier>();
				break;
			}
			square.GetComponent<BoardSquare>().Number=currentLevel[i];
			square.transform.position = new Vector3 (-4.5f+column*(square.GetComponent<Renderer>().bounds.size.x),
			                                        3.5f - ((square.GetComponent<Renderer>().bounds.size.y) * rows),
			                                        0);

			column++;
			if (column==10) {
				column = 0;
				rows++;
			}			
		}

		addAeroPlaneSquareToEmptySeaSquare();
	}

	public void shipsLeftCounter() {
		ShipsLeft--;
		shipsLeftTextBox.text = "Ships Left: " + ShipsLeft;
	}

	//this method can be called to update shots. It is already called when a square is selected - see BoardSquare.cs
	public void shotsLeftCounter(int value)
	{
        ShotsLeft--;
        shotsLeftTextBox.text = "shots left: " + ShotsLeft;  
	}

	public void addAeroPlaneSquareToEmptySeaSquare()
	{
		Sea[] squares = FindObjectsOfType<Sea>() as Sea[];

		//add component aeroplane to squares
		GameObject tile = squares[Random.Range(0, squares.Length)].gameObject;
		Destroy(tile.GetComponent<Sea>()); 
		tile.AddComponent<AeroPlane>();
		tile.name = "AeroPlane";	
	}
}
